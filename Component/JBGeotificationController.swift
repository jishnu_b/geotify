//
//  JBGeotificationController.swift
//  Geo
//
//  Created by Jishnu on 22/12/16.
//  Copyright © 2016 Jishnu Inc. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation


class  JBGeotificationController : NSObject,CLLocationManagerDelegate {
    
    var geotifications :[Geotification]?
    let locationManager = CLLocationManager()
    var noMonitoringAvailableBlock:(()->())?
    var noMonitoringPermissionAvailableBlock:(()->())?
    var geotificationTriggered:((_ triggeredGeotification:Geotification)->())?
    
    
    
    convenience init(InitWithGeotifications geotifications:[Geotification],DoWhenNoMonitoringAvailable noMonitoringAvailableBlock:@escaping ()->(),DoWheNoMonitoringPermissionAvailable     noMonitoringPermissionAvailableBlock:@escaping ()->(),DoWhenGeotificationTriggered geotificationTriggered:@escaping (_ triggeredGeotification:Geotification)->()
        ) {
        self.init()
        self.geotifications = geotifications
        self.noMonitoringAvailableBlock = noMonitoringAvailableBlock
        self.noMonitoringPermissionAvailableBlock = noMonitoringPermissionAvailableBlock
        self.geotificationTriggered = geotificationTriggered
        setUpLocManager()
    }
    
    
    func setUpLocManager(){
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
    }

    
    
    func startMonitoringAfterCheckingMonitoringAvailablilityAndPermission() {
        if !CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            self.noMonitoringAvailableBlock!()
            return
        }
        if CLLocationManager.authorizationStatus() != .authorizedAlways {
            self.noMonitoringPermissionAvailableBlock!()
            return
        }
        startMonitoringEachGeotifications()
    }
    
    func startMonitoringEachGeotifications(){
        guard  let geotifications = self.geotifications, geotifications.count>0  else {return}
        for geotificationObj in geotifications {
            let region = getRegion(withGeotification: geotificationObj)
            locationManager.startMonitoring(for: region)
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        triggerGeotification(ForRegion: region)
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        triggerGeotification(ForRegion: region)
    }
    
    func   triggerGeotification(ForRegion region:CLRegion) {
        guard let regionAsCircular = region as? CLCircularRegion  else {return}
        guard  let triggeredGeotification = getGeotification(withRegion:regionAsCircular, FromGeotifications: self.geotifications!) else {return}
        self.geotificationTriggered!(triggeredGeotification)
    }
}




func getRegion(withGeotification geotification: Geotification) -> CLCircularRegion {
    let region = CLCircularRegion(center: geotification.coordinate, radius: geotification.radius, identifier: geotification.identifier)
    region.notifyOnEntry = (geotification.eventType == .onEntry || geotification.eventType == .onBothEntryAndExit )
    region.notifyOnExit = (geotification.eventType == .onExit || geotification.eventType == .onBothEntryAndExit )
    return region
}


func getGeotification(withRegion region: CLCircularRegion,FromGeotifications geotifications:[Geotification]) -> Geotification? {
    for geotification in geotifications {
        if (geotification.identifier == region.identifier){
            return geotification
        }
        return nil
    }
    return nil
}
