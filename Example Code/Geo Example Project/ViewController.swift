//
//  ViewController.swift
//  Geo
//
//  Created by Jishnu on 21/12/16.
//  Copyright © 2016 Jishnu Inc. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController {
 
    var geotificationController:JBGeotificationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       doGeoFencing()
    }
   
    func doGeoFencing(){
        
        geotificationController = JBGeotificationController(InitWithGeotifications:getGeotificationObjects(), DoWhenNoMonitoringAvailable: {
            ()  in
            self.doWhenMonitoringNotAvailable()
        }, DoWheNoMonitoringPermissionAvailable: {
            ()  in
            self.doWhenMonitoringPermissionNotAvailable()
        }, DoWhenGeotificationTriggered: {
            (geotificationTriggered) in
            self.GeotificationTriggered(ForGeotification:geotificationTriggered )
        })
        
        geotificationController?.startMonitoringAfterCheckingMonitoringAvailablilityAndPermission()
    }
    
    func getGeotificationObjects()->[Geotification]{
        //As of now pass single geotification.
        let coordinate = CLLocationCoordinate2DMake(37.422, -122.084058)
        let geotification = Geotification(coordinate: coordinate, radius: 100, identifier: "id", note: "hi", eventType: .onEntry)
        return [geotification]
    }
    
    func doWhenMonitoringNotAvailable(){
        showAlert(withTitle:"Error", message: "Geofencing is not supported on this device!")
    }
    
    func doWhenMonitoringPermissionNotAvailable(){
        showAlert(withTitle:"Warning", message: "Your geotification is saved but will only be activated once you grant Geotify permission to access the device location.")
    }

    func GeotificationTriggered(ForGeotification geotification:Geotification){
        if UIApplication.shared.applicationState == .active {
            let message = geotification.identifier;
            showAlert(withTitle: nil, message: message)
        } else {
            // Otherwise present a local notification
            let notification = UILocalNotification()
            notification.alertBody =  geotification.identifier
            notification.soundName = "Default"
            UIApplication.shared.presentLocalNotificationNow(notification)
        }
    }
   
}
extension UIViewController{
    func showAlert(withTitle title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
}

